= Hippo Platform (Local)

https://docs.hippofactory.dev/intro/quickstart/

== Prerequisites

1. Install Rust (https://www.rust-lang.org/tools/install)
2. Install Hippo Cli (https://docs.hippofactory.dev/intro/install-hippo/)
3. Install Spin (https://developer.fermyon.com/spin/install#using-cargo-to-install-spin)

== Start Hippo Platform

.Start Bindle and Hippo
[source,bash]
----
podman-compose build
podman-compose up -d
----